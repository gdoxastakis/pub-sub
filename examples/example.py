from pubsub import main_broker
from timeit import default_timer as timer

if __name__ == "__main__":
    sub1 = main_broker.subscribe(['test1', 'test2'])
    sub2 = main_broker.subscribe(['test3'])
    publisher1 = main_broker.get_publisher('test1')
    publisher2 = main_broker.get_publisher('test2')
    publisher3 = main_broker.get_publisher('test3')
    start = timer()
    m1 = publisher1.put(dict(test=1))
    m2 = publisher2.put(dict(test=2))
    m3 = publisher3.put(dict(test=3))
    print('sub1', sub1.get().content, sub1.get().content)
    print("sub2", sub2.get().content)
    print('Elapsed {:.15f}'.format(timer()-start))
