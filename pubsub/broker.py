import threading
from queue import PriorityQueue, Empty, Full
import logging
from typing import List
from .message import Message, Priority


class PubSubBroker(threading.Thread):
    def __init__(self):
        super().__init__(name='PubSubBroker', daemon=True)
        self.logger = logging.getLogger(self.name)
        self._stop_event = threading.Event()
        self.publish_queue = PriorityQueue()
        self.subscribers = []

    def stop(self):
        self._stop_event.set()

    @property
    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        self.logger.info(f"Broker {self.name} has started")
        while not self.stopped:
            try:
                self.process_messages()
            except Empty:
                pass
            except Exception as exc:
                self.logger.error(f"Broker {self.name} failed to process message error: {repr(exc)}")

    def publish(self, message: Message):
        self.publish_queue.put(message, timeout=10)

    def subscribe(self, topics: List[str], queue_maxsize: int = 0):
        subscriber = Subscriber(topics, queue_maxsize)
        self.subscribers.append(subscriber)
        return subscriber

    def get_publisher(self, topic: str):
        return Publisher(topic, self)

    def process_messages(self):
        message = self.publish_queue.get(timeout=5)
        self.publish_queue.task_done()
        recipients = [subscriber for subscriber in self.subscribers if message.topic in subscriber.topics]
        for recipient in recipients:
            try:
                recipient.queue.put(message, block=False)
                message.delivered = True
            except Full:
                pass


class Subscriber:
    def __init__(self, topics: List[str], queue_maxsize: int = 0):
        self.topics = topics
        self.queue = PriorityQueue(maxsize=queue_maxsize)
        self.subscribed = True

    def get(self, block=True, timeout=None):
        message = self.queue.get(block, timeout)
        self.queue.task_done()
        return message


class Publisher:
    def __init__(self, topic: str, broker: PubSubBroker):
        self.topic = topic
        self.broker = broker

    def put(self, content, priority=Priority.NORMAL) -> Message:
        message = Message(content=content, topic=self.topic, priority=priority)
        self.broker.publish(message)
        return message
