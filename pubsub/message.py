from enum import Enum
from typing import Callable
from functools import total_ordering
import threading

import time


class Priority(Enum):
    HIGHEST = 0
    HIGH = 1
    NORMAL = 2
    LOW = 3
    LOWEST = 4


@total_ordering
class Message:
    def __init__(self, content: dict, topic: str, priority: Priority = Priority.NORMAL,
                 response_callback: Callable[..., None] = None):
        self.content = content
        self.priority = priority
        self.timestamp = time.time()
        self.topic = topic
        self.response_callback = response_callback
        self.delivered = False
        self._response_event = threading.Event()
        self._response = None

    def __lt__(self, other):
        return (self.priority.value, self.timestamp) < (other.priority.value, other.timestamp)

    def respond(self, response):
        self._response = response
        self._response_event.set()

    def get_response(self, block=True, timeout=None):
        if block:
            self._response_event.wait(timeout=timeout)
        return self._response
