from .broker import PubSubBroker

main_broker = PubSubBroker()
main_broker.start()
